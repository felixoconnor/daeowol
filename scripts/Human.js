class Human extends GameObject {
    constructor(props) {
        super(props);
        this.movingProgressRemaining = 0;
        this.isPlayer = props.isPlayer || false;
        this.directionUpdate = {
            'up': ['y', -1],
            'down': ['y', 1],
            'left': ['x', -1],
            'right': ['x', 1],
        }
    }

    update(state) {
        if (this.movingProgressRemaining > 0) {
            this.updatePosition();
        } else {
            if (this.isPlayer && state.arrow) {
                this.startAction(state, {
                    type: 'walk',
                    direction: state.arrow
                })
            }
            this.updateSprite(state);
        }
    }

    updatePosition() {
        const [property, change] = this.directionUpdate[this.direction];
        this[property] += change;
        this.movingProgressRemaining -= 1;

    }

    updateSprite() {
        if (this.movingProgressRemaining > 0) {
            this.sprite.setAnimation('walk-' + this.direction);
            return;
        }
        this.sprite.setAnimation('idle-' + this.direction);
    }

    startAction(state, action) {
        this.direction = action.direction;
        if (action.type === 'walk') {
            if (state.map.isPositionTaken(this.x, this.y, this.direction)) {
                return;
            }
            state.map.moveWall(this.x, this.y, this.direction);
            this.movingProgressRemaining = 16;
        }
    }
}