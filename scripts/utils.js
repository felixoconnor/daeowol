const utils = {
    grid(n) {
        return n * 16;
    },

    gridPosition(x, y) {
        return `${x*16}, ${y*16}`;
    },

    nextPosition(currentX, currentY, direction) {
        let x = currentX;
        let y = currentY;
        const size = 16;

        if (direction === 'left') {
            x -= size;
        } else if (direction === 'right') {
            x += size;
        } else if (direction === 'up') {
            y -= size;
        } else if (direction === 'down') {
            y += size;
        }

        return {x, y};
    }
}