class Map {
    constructor(props) {
        this.gameObjects = props.gameObjects;
        this.walls = props.walls || {};
        this.lowerImage = new Image();
        this.lowerImage.src = props.lowerSrc;
        this.upperImage = new Image();
        this.upperImageSrc = props.upperSrc;
    }

    drawLowerImage(ctx) {
        ctx.drawImage(this.lowerImage, 0, 0);
    }

    drawUpperImage(ctx) {
        ctx.drawImage(this.upperImage, 0, 0);
    }

    isPositionTaken(currentX, currentY, direction) {
        const { x, y } = utils.nextPosition(currentX, currentY, direction);
        return this.walls[`${x}, ${y}`] || false;
    }

    mountObjects() {
        Object.values(this.gameObjects).forEach(object => {
            object.mount(this);
        })
    }

    addWall(x, y) {
        this.walls[`${x}, ${y}`] = true;
    }

    removeWall(x, y) {
        delete this.walls[`${x}, ${y}`];
    }

    moveWall(currentX, currentY, direction) {
        this.removeWall(currentX, currentY);
        const {x, y} = utils.nextPosition(currentX, currentY, direction);
        this.addWall(x, y);
    }
}

window.Maps = {
    Demo: {
        lowerSrc: 'images/maps/lower.png',
        upperSrc: 'images/maps/upper.png',
        gameObjects: {
            player: new Human({
                x: utils.grid(5),
                y: utils.grid(5),
                isPlayer: true,
            }),
            someone: new Human({
                x: utils.grid(7),
                y: utils.grid(7),
                src: 'images/characters/npc.png',
            })
        },
        walls: {
            // Wall on the left
            [utils.gridPosition(0, 0)]: true,
            [utils.gridPosition(0, 1)]: true,
            [utils.gridPosition(0, 2)]: true,
            [utils.gridPosition(0, 3)]: true,
            [utils.gridPosition(0, 4)]: true,
            [utils.gridPosition(0, 5)]: true,
            [utils.gridPosition(0, 6)]: true,
            [utils.gridPosition(0, 7)]: true,
            [utils.gridPosition(0, 8)]: true,
            [utils.gridPosition(0, 9)]: true,
            [utils.gridPosition(0, 10)]: true,

            // Wall on top
            [utils.gridPosition(0, 0)]: true,
            [utils.gridPosition(1, 0)]: true,
            [utils.gridPosition(2, 0)]: true,
            [utils.gridPosition(3, 0)]: true,
            [utils.gridPosition(4, 0)]: true,
            [utils.gridPosition(5, 0)]: true,
            [utils.gridPosition(6, 0)]: true,
            [utils.gridPosition(7, 0)]: true,
            [utils.gridPosition(8, 0)]: true,
            [utils.gridPosition(9, 0)]: true,
            [utils.gridPosition(10, 0)]: true,

            // Black object
            [utils.gridPosition(3, 3)]: true,
            [utils.gridPosition(2, 3)]: true,
            [utils.gridPosition(3, 2)]: true,
            [utils.gridPosition(2, 2)]: true,
        }
    }
}