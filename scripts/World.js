class World {
    constructor() {
        this.element = document.querySelector('.game-container');
        this.canvas = this.element.querySelector('.game-canvas');
        this.ctx = this.canvas.getContext('2d');
        this.map = null;
    }

    // Game loop
    loop() {
        const step = () => {
            // Clear off the world
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

            // Lower world layer
            this.map.drawLowerImage(this.ctx);

            // Game objects
            Object.values(this.map.gameObjects).forEach(object => {
                object.update({
                    arrow: this.direction.dir,
                    map: this.map
                });
                object.sprite.draw(this.ctx);
            })

            // Upper world layer
            this.map.drawUpperImage(this.ctx);

            requestAnimationFrame(() => {
                step();
            })
        }
        step();
    }

    // Initialize the game
    init() {
        this.map = new Map(window.Maps.Demo);
        this.map.mountObjects();
        this.direction = new Direction();
        this.direction.init();
        this.loop();
    }
}