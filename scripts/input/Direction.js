class Direction {
    constructor() {
        this.directions = [];
        this.map = {
            'ArrowUp': 'up',
            'ArrowDown': 'down',
            'ArrowLeft': 'left',
            'ArrowRight': 'right',
            'KeyW': 'up',
            'KeyS': 'down',
            'KeyA': 'left',
            'KeyD': 'right',
        }
    }

    get dir() {
        return this.directions[0];
    }

    init() {
        document.addEventListener('keydown', e => {
            const dir = this.map[e.code];
            if (dir && this.directions.indexOf(dir) === -1) {
                this.directions.unshift(dir);
            }
        }),
        document.addEventListener('keyup', e => {
            const dir = this.map[e.code];
            const index = this.directions.indexOf(dir);
            if (index > -1) {
                this.directions.splice(index, 1);
            }
        })
    }
}
