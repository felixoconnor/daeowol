class GameObject {
    constructor(props) {
        this.mounted = false;
        this.x = props.x || 0;
        this.y = props.y || 0;
        this.direction = props.direction || 'down';
        this.sprite = new Sprite({
            gameObject: this,
            src: props.src || 'images/player/player.png',
        });
    }

    mount(map) {
        this.mounted = true;
        map.addWall(this.x, this.y);
    }

    update() { }
}